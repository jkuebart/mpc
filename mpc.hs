import Data.Char (ord)
import Prelude hiding ((<*>), (<|>), fmap, return)

-- Our own StateT
import StateT

type Parser = StateT String []

runParser :: Parser a -> String -> [(a, String)]
runParser = runStateT

-- The parser that parses one item
item :: Parser Char
item = StateT (\inp -> case inp of
               [] -> []
               (x:xs) -> [(x, xs)])


-- 
sat :: (Char -> Bool) -> Parser Char
sat p = StateT (\inp -> case inp of
    "" -> []
    (x:xs) -> if p x then [(x, xs)] else [])

-- Character parsers
char x = sat (\y -> x == y)
digit = sat (\x -> '0' <= x && x <= '9')
lower = sat (\x -> 'a' <= x && x <= 'z')
upper = sat (\x -> 'A' <= x && x <= 'Z')
letter = lower <|> upper
alphanum = letter <|> digit

string :: String -> Parser String
string "" = return ""
string (x:xs) = fmap (:) (char x) <*> string xs

-- many is inherited from Alternative
word :: Parser String
word = many letter

-- some is inherited from Alternative
nat :: Parser Int
nat = fmap (foldl (\n d -> 10 * n + ord d - ord '0') 0) (some digit)

int :: Parser Int
int = nat <|> (fmap (\_ n -> -n) (char '-') <*> nat)

main = do
    print (runParser item "abc")
    print $ runParser letter "abc"
    print $ runParser letter "Abc"
    print $ runParser letter "123"
    print $ runParser nat "123"
    print $ runParser nat "-123"
    print $ runParser int "-123"
