module StateT where

import Prelude ((.), (++))


{-

The Functor type class.

-}

class Functor m where
    fmap :: (a -> b) -> m a -> m b

instance Functor [] where
    fmap f xs = [ f x | x <- xs ]


{-

The Applicative type class.

-}

class Functor f => Applicative f where
    return :: a -> f a
    (<*>) :: f (a -> b) -> f a -> f b

instance Applicative [] where
    return v = [v]
    fs <*> xs = [ f x | f <- fs, x <- xs ]


{-

The Monad type class.

-}

class Applicative m => Monad m where
    (>>=) :: m a -> (a -> m b) -> m b

instance Monad [] where
    xs >>= f = [ x' | x <- xs, x' <- f x ]


{-

A List type.

-}

newtype List a = List { toArray :: [a] }

instance Functor List where
    fmap f (List l) = List [ f x | x <- l ]

instance Applicative List where
    return v = List [v]
    List fs <*> List xs = List [ f x | f <- fs, x <- xs ]

instance Monad List where
    -- (>>=) :: List a -> (a -> List b) -> List b
    --l >>= f = List (concat [toArray (f a) | a <- toArray l])
    List l >>= f = List [ x' | x <- l, x' <- toArray (f x) ]


{-

A simple State type.

-}

-- runState :: State s a -> s -> (a, s)
newtype State s a = State { runState :: s -> (a, s) }

updateS :: (s -> s) -> State s s
updateS f = State (\s -> (s, f s))

instance Functor (State s) where
    fmap f (State x) = State (\s -> let (a, s') = x s in (f a, s'))

instance Applicative (State s) where
    return v = State (\s -> (v, s))
    State fs <*> State xs =
        State (\s ->
            let
                (f, s') = fs s
                (x, s'') = xs s'
            in
                (f x, s''))

instance Monad (State s) where
    -- (>>=) :: State s a -> (a -> State s b) -> State s b
    State m >>= f = State (\s -> let (a, s') = m s in runState (f a) s')


{-

The StateT transformer.

-}

newtype StateT s m a = StateT { runStateT :: s -> m (a, s) }

update :: (Monad m) => (s -> s) -> StateT s m s
update f = StateT (\s -> return (s, f s))

instance Functor m => Functor (StateT s m) where
    -- fmap :: (a -> b) -> m a -> m b
    fmap f (StateT x) = StateT (\s -> fmap (\(a, s') -> (f a, s')) (x s))

instance Monad m => Applicative (StateT s m) where
    return v = StateT (\s -> return (v, s))

    StateT mf <*> StateT ma =
        StateT (\s -> mf s >>= \(f, s') -> fmap (\(a, s'') -> (f a, s'')) (ma s'))

instance Monad m => Monad (StateT s m) where
    -- >>= :: Monad (StateT s m a) -> (a -> Monad (StateT s m b)) -> Monad (StateT s m b)
    StateT m >>= f = StateT (\s -> m s >>= \(a, s') -> runStateT (f a) s')


{-

The Alternative type class

-}

class Applicative f => Alternative f where
    (<|>) :: f a -> f a -> f a

    many :: f a -> f [a]
    some :: f a -> f [a]
    many p = some p <|> return []
    some p = fmap (:) p <*> many p

instance Alternative [] where
    (<|>) = (++)

instance (Alternative m, Monad m) => Alternative (StateT s m) where
    StateT p <|> StateT q = StateT (\s -> p s <|> q s)
